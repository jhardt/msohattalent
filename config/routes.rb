Rails.application.routes.draw do
  scope "/auth" do
    devise_for :users, controllers: {
      registrations: 'users/registrations'
    }
  end
  
  get '/login', to: redirect('auth/users/sign_in')
  get '/lpr' => 'home#landing_page_redirect'

  get 'v' => 'audience_votes#new', as: :new_audience_vote
  post 'v' => 'audience_votes#create', as: :create_audience_vote
  get 'v/thankyou' => 'audience_votes#thankyou', as: :audience_vote_thankyou

  get 'orders/new'
  post 'orders/create'
  get 'orders/confirm_email'
  get 'orders/print_tickets'
  get 'orders/thankyou'
  get 'orders/verify_email'

  get 'settings' => 'settings#modify'
  post 'settings' => 'settings#update', as: :update_settings

  get 'home/index'
  resources :participants do
    collection do
      post 'update_order'
    end
  end
  resources :ticket_orders do
    collection do
      post 'admission_fee_paid'
    end
  end
  resources :tickets, only: [:index, :destroy] do
    collection do
      get 'print_tickets'
      post 'batch_create'
      get 'ticket_inspection'
      post 'checkin'
    end
    member do
      get 'image'
    end
  end
  resources :votes, only: :index
  resources :jurors, only: [:create, :update, :destroy]

  root to: 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
