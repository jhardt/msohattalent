# Preview all emails at http://localhost:3000/rails/mailers/order_mailer
class OrderMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/confirmation_email
  def confirmation_email
    OrderMailer.confirmation_email
  end

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/ticket_email
  def ticket_email
    OrderMailer.ticket_email
  end

end
