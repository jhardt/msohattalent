require 'test_helper'

class UserVotesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get user_votes_new_url
    assert_response :success
  end

  test "should get thankyou" do
    get user_votes_thankyou_url
    assert_response :success
  end

end
