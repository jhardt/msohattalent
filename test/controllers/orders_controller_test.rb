require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get orders_new_url
    assert_response :success
  end

  test "should get thankyou" do
    get orders_thankyou_url
    assert_response :success
  end

  test "should get verify_email" do
    get orders_verify_email_url
    assert_response :success
  end

end
