require 'test_helper'

class JurorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @juror = jurors(:one)
  end

  test "should get index" do
    get jurors_url
    assert_response :success
  end

  test "should get new" do
    get new_juror_url
    assert_response :success
  end

  test "should create juror" do
    assert_difference('Juror.count') do
      post jurors_url, params: { juror: { name: @juror.name, participant_id: @juror.participant_id } }
    end

    assert_redirected_to juror_url(Juror.last)
  end

  test "should show juror" do
    get juror_url(@juror)
    assert_response :success
  end

  test "should get edit" do
    get edit_juror_url(@juror)
    assert_response :success
  end

  test "should update juror" do
    patch juror_url(@juror), params: { juror: { name: @juror.name, participant_id: @juror.participant_id } }
    assert_redirected_to juror_url(@juror)
  end

  test "should destroy juror" do
    assert_difference('Juror.count', -1) do
      delete juror_url(@juror)
    end

    assert_redirected_to jurors_url
  end
end
