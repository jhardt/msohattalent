json.extract! participant, :id, :name, :description, :yt_video_id, :created_at, :updated_at
json.url participant_url(participant, format: :json)
