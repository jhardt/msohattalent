json.extract! ticket_order, :id, :number_of_tickets, :email, :email_verified, :created_at, :updated_at
json.url ticket_order_url(ticket_order, format: :json)
