class OrderMailer < ApplicationMailer
  default from: 'no-reply@mso-abi-2018.de'

  def confirmation_email(order)
    @order = order

    mail to: order.email, subject: "MSO hat Talent: E-Mail-Adresse bestätigen"
  end

  def ticket_email(order)
    @order = order
    attachments["MSOHatTalent-Karten.pdf"] = { :mime_type => 'application/pdf', :content => TicketGenerator.generate_order_pdf(order).render }

    mail to: order.email, subject: "MSO hat Talent: Ihre Karten"
  end
end
