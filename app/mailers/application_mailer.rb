class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@mso-abi-2018.de'
  layout 'mailer'
end
