module ApplicationHelper
    def is_number? string
      true if Float(string) rescue false
    end
  
    def is_i? string
      !!(string =~ /\A[-+]?[0-9]+\z/)
    end
    
    def flash_class(level)
      case level
        when :notice then "info"
        when :error then "danger"
        when :alert then "warning"
      end
    end
  
    def format_decimal(value)
      value.to_s.gsub(/\./, ",")
    end
    
    def format_currency(value, options = {})
      number_to_currency(value, { locale: :de, raise: true }.merge(options))
    end
  
    def format_tick(value)
      value ? '✔' : '✘'
    end
  
    def format_datetime(datetime)
      datetime.present? ? datetime.strftime("%d.%m.%Y %H:%M") : "-"
    end
  
    def format_date(datetime)
      datetime.present? ? datetime.strftime("%d.%m.%Y") : "-"
    end
  
    def format_time(datetime)
      datetime.present? ? datetime.strftime("%H:%M") : "-"
    end
  
    def format_text(text)
      text = text.gsub(/\n/, '<br />')
      text = sanitize(text, tags: %w(br))
      markdown(text)
    end
  
    def markdown(text)
      markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)
      markdown.render(text).html_safe
    end
  
    def glyphicon_with_text(glyph = :leaf, text = "", options = {})
      glyphicon(glyph, options) + " " + text
    end
  
    def glyphicon(glyph = :leaf, options = {})
      content_tag(:span, nil, { class: "glyphicon glyphicon-#{glyph.to_s}" }.merge(options))
    end
  
    def bs_label(context, content = nil)
      content_tag(:span, content, class: "label label-#{context}")
    end
  
    def new_button(path, label = nil, options = {})
      link_to glyphicon(:plus) + (label.present? ? ' ' : nil) + label, path, { class: 'btn btn-xs btn-default' }.merge(options)
    end
  
    def details_button(path, options = {})
      link_to glyphicon('th-list'), path, { class: 'btn btn-xs btn-default' }.merge(options)
    end
  
    def edit_button(path, options = {})
      link_to glyphicon(:pencil), path, { class: 'btn btn-xs btn-primary' }.merge(options)
    end
  
    def destroy_button(path, options = {})
      link_to glyphicon(:trash), path, { method: :delete, data: { confirm: 'Bist Du sicher?' }, class: 'btn btn-xs btn-danger' }.merge(options)
    end
  
    def cancel_button(path, text = "Cancel", options = {})
      link_to text, path, { class: "btn btn-default" }.merge(options)
    end
  end
  