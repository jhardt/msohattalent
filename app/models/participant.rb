class Participant < ApplicationRecord
    has_many :votes, dependent: :destroy
    has_many :jurors, dependent: :nullify

    has_attached_file :picture

    validates :name, presence: true
    validates_attachment :picture, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
end
