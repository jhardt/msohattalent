class Settings < ApplicationRecord
    enum round: [:disabled, :qualifying_round, :final, :sale_of_tickets]

    after_update :set_finalists

    validates :round, presence: true
    validates :max_online_tickets, presence: true

    def set_finalists
        if round_changed? && round == "final"
            # Set finalists
            Participant.update_all(is_finalist: false)
            participant_ids = Vote.joins(:participant).where(round: :qualifying_round).select("participant_id, COUNT(*) AS number_of_votes").group("participants.id").order("number_of_votes DESC").first(8).map(&:participant_id)
            Participant.where(id: participant_ids).update_all(is_finalist: true)
            
            # Clear jury's recommendations
            Juror.update_all(participant_id: nil)
        end
    end

    def self.get
        Settings.first || Settings.new
    end
end
