class User < ApplicationRecord
  enum role: [:ticket_inspector, :admin]
  devise :database_authenticatable, :registerable, :rememberable, :trackable, :validatable
end
