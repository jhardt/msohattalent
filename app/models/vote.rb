class Vote < ApplicationRecord
  enum round: [:qualifying_round, :final]

  belongs_to :ticket
  belongs_to :participant
end
