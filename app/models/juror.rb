class Juror < ApplicationRecord
  belongs_to :participant, optional: true

  validates :name, presence: true, uniqueness: true
end
