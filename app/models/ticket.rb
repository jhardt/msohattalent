class Ticket < ApplicationRecord
    has_many :votes, dependent: :destroy
    belongs_to :ticket_order, optional: true
    scope :online_tickets, -> { where.not(ticket_order_id: nil) }
    scope :offline_tickets, -> { where(ticket_order_id: nil) }

    before_create :set_code

    def set_code
        characters = ('A'..'Z').to_a + ('a'..'z').to_a + ('0'..'9').to_a
        begin
            self.code = (0...5).map { characters.sample(random: SecureRandom) }.join
        end while Ticket.find_by(code: code).present?
        true
    end
end
