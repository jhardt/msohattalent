class TicketOrder < ApplicationRecord
    has_many :tickets, dependent: :destroy

    before_create :set_email_verification_token

    validates :first_name, presence: true
    validates :last_name, presence: true
    validates :number_of_tickets, presence: true
    validates :email, presence: true, format: { with: Devise.email_regexp }    

    def set_email_verification_token
        begin
            self.email_verification_token = SecureRandom.hex
        end while TicketOrder.find_by(email_verification_token: email_verification_token).present?
        true
    end
end
