$(function() {
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', token);
        }
    });
});

$(document).on('ready turbolinks:load', function() {
    $('#participants').sortable();
    $('#participants').disableSelection();

    $('#participants').on('sortupdate', function(event, ui) {
        var participant_ids = $('#participants').find('tr').toArray().map(function(tr) {
            return tr.getAttribute('data-id');
        });
        $.post(this.getAttribute('data-update-order-href'), { 'participant_ids': participant_ids });
    });
});