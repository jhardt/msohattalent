require 'chunky_png/rmagick'

class TicketGenerator
    include Magick
    
    def self.generate_order_pdf(order)
        pdf = Prawn::Document.new
        for ticket in order.tickets
            pdf.image(StringIO.new(TicketGenerator.generate_ticket_jpeg(ticket)), scale: 0.25)
        end
        pdf
    end

    def self.generate_ticket_jpeg(ticket)
        txt_vote_url = UrlGenerator.new.new_audience_vote_url
        txt_vote_url_with_code = UrlGenerator.new.new_audience_vote_url(code: ticket.code)
        txt_ticket_code = "Code: #{ticket.code}"
        txt_ticket_type = ticket.ticket_order.present? ? "Online-Karte" : "VVK-Karte"
        txt_payment_hint = ticket.ticket_order.present? ? "3€ am Einlass zu entrichten" : "Karte ist bezahlt"
        txt_vote = "Abstimmung:"
        txt_place_time_info = "01.12.2017 18:00 Aula der Marienschule Opladen"

        img = Image.read(Rails.root.join('app', 'assets', 'images', 'MSOHatTalent-Karte.jpg'))[0]
        
        # QR-Code
        qrcode = RQRCode::QRCode.new(txt_vote_url_with_code)
        qr = ChunkyPNG::RMagick.export(qrcode.as_png(size: 400))
        img.composite!(qr, CenterGravity, 0, 275, OverCompositeOp)
        
        # VVK-Karte / Online-Karte
        txt = Draw.new
        img.annotate(txt, 0, 0, 0, -130, txt_ticket_type) {
            txt.gravity = Magick::CenterGravity
            txt.pointsize = 100
            txt.fill = "#000000"
        }
        
        # Entgelt bezahlt / 3€ am Einlass zu entrichten
        txt = Draw.new
        img.annotate(txt, 0, 0, 0, -50, txt_payment_hint) {
            txt.gravity = Magick::CenterGravity
            txt.pointsize = 35
            txt.fill = "#000000"
        }

        # 01.12.2017 18:00 Aula der Marienschule Opladen
        txt = Draw.new
        img.annotate(txt, 0, 0, 0, 10, txt_place_time_info) {
            txt.gravity = Magick::CenterGravity
            txt.pointsize = 25
            txt.fill = "#000000"
        }
        
        # Abstimmung
        txt = Draw.new
        img.annotate(txt, 0, 0, 0, 75, txt_vote) {
            txt.gravity = Magick::CenterGravity
            txt.pointsize = 40
            txt.fill = "#000000"
        }
        
        # URL
        txt = Draw.new
        img.annotate(txt, 0, 0, 0, 500, txt_vote_url) {
            txt.gravity = Magick::CenterGravity
            txt.pointsize = 30
            txt.fill = "#000000"
        }
        
        # Code
        txt = Draw.new
        img.annotate(txt, 0, 0, 0, 540, txt_ticket_code) {
            txt.gravity = Magick::CenterGravity
            txt.pointsize = 30
            txt.fill = "#000000"
        }

        img.format = "jpeg"       
        img.to_blob
    end
end