class HomeController < ApplicationController
  def index
  end

  def landing_page_redirect
    redirect_to orders_new_path
  end

  protected
    def set_active_item
      @active = :home
    end
end
