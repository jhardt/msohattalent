class Users::RegistrationsController < Devise::RegistrationsController
    def new
        flash[:alert] = "Permission denied."
        redirect_to root_url
    end

    def create
        flash[:alert] = "Permission denied."
        redirect_to root_url
    end
end