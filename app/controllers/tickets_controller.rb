class TicketsController < ApplicationController
  include ApplicationHelper
  load_and_authorize_resource

  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Ticket.order(created_at: :desc)
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Das Ticket wurde gelöscht.' }
      format.json { head :no_content }
    end
  end

  def print_tickets
    if params[:ticket_ids].blank? || !params[:ticket_ids].is_a?(Array)
      render nothing: true
    else
      @tickets = params[:ticket_ids].map { |id| Ticket.find(id) }
    end
  end

  def image
    ticket = Ticket.find(params[:id])
    if ticket
      send_data TicketGenerator.generate_ticket_jpeg(ticket), :type => "image/jpeg", :disposition => "inline"
    else
      render nothing: true
    end
  end

  def batch_create
    if is_number?(params[:amount]) 
      tickets = []
      params[:amount].to_i.times { tickets << Ticket.create(admission_fee_paid: true) }
      redirect_to print_tickets_tickets_path(ticket_ids: tickets.map(&:id))
    else
      render nothing: true
    end
  end

  def ticket_inspection
    @active = :ticket_inspection
    @no_header = true
  end

  def checkin
    if params[:ticket_code].blank? || (ticket = Ticket.find_by_code(params[:ticket_code])).blank?
      render json: { status: "ticket_not_found" }
    elsif ticket.checked_in?
      render json: { status: "ticket_already_checked_in" }
    else
      admission_fee_paid = ticket.admission_fee_paid?
      order = ticket.ticket_order.blank? ? nil : { id: ticket.ticket_order.id, remaining_admission_fee: ticket.ticket_order.tickets.where(admission_fee_paid: false).count * 3 }
      ticket.update_attribute :checked_in, true
      ticket.update_attribute :admission_fee_paid, true
      render json: { status: "checkin_successful", admission_fee_paid: admission_fee_paid, order: order }      
    end
  end

  protected
    def set_active_item
      @active = :tickets
    end
end
