class SettingsController < ApplicationController
  def modify
    authorize! :modify, Settings
    @settings = Settings.get
    @jurors = Juror.all
    @juror = Juror.new
  end

  def update
    authorize! :update, Settings
    Settings.get.update(settings_params)
    flash[:notice] = "Die Einstellungen wurden aktualisiert."
    redirect_to settings_path
  end

  protected
    def set_active_item
      @active = :settings
    end

  private
    def settings_params
      params.require(:settings).permit(:round, :max_online_tickets)
    end
end
