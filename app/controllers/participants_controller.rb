class ParticipantsController < ApplicationController
  load_and_authorize_resource

  # GET /participants
  # GET /participants.json
  def index
    @participants = Participant.order(:position)
  end

  # GET /participants/1
  # GET /participants/1.json
  def show
  end

  # GET /participants/new
  def new
    @participant = Participant.new
  end

  # GET /participants/1/edit
  def edit
  end

  # POST /participants
  # POST /participants.json
  def create
    @participant = Participant.new(participant_params)

    respond_to do |format|
      if @participant.save
        format.html { redirect_to @participant, notice: 'Die TeilnehmerIn(en) wurden erstellt.' }
        format.json { render :show, status: :created, location: @participant }
      else
        format.html { render :new }
        format.json { render json: @participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /participants/1
  # PATCH/PUT /participants/1.json
  def update
    respond_to do |format|
      if @participant.update(participant_params)
        format.html { redirect_to @participant, notice: 'Die TeilnehmerIn(en) wurden aktualisiert.' }
        format.json { render :show, status: :ok, location: @participant }
      else
        format.html { render :edit }
        format.json { render json: @participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /participants/1
  # DELETE /participants/1.json
  def destroy
    @participant.destroy
    respond_to do |format|
      format.html { redirect_to participants_url, notice: 'Die TeilnehmerIn(en) wurden gelöscht.' }
      format.json { head :no_content }
    end
  end

  def update_order
    params[:participant_ids].each_with_index do |participant_id, position|
      Participant.find(participant_id).try(:update_attribute, :position, position)
    end
  end

  protected
    def set_active_item
      @active = :participants
    end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def participant_params
      params.require(:participant).permit(:name, :description, :yt_video_id, :picture)
    end
end
