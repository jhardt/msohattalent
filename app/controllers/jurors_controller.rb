class JurorsController < ApplicationController
  load_and_authorize_resource
  # before_action :set_juror, only: [:show, :edit, :update, :destroy]

  # POST /jurors
  # POST /jurors.json
  def create
    @juror = Juror.new(juror_params)

    respond_to do |format|
      if @juror.save
        format.html { redirect_to settings_path, notice: 'Das Jury-Mitglied wurde erfolgreich erstellt.' }
        format.json { render :show, status: :created, location: @juror }
      else
        format.html { render "settings/modify" }
        format.json { render json: @juror.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jurors/1
  # PATCH/PUT /jurors/1.json
  def update
    respond_to do |format|
      if @juror.update(juror_params)
        format.html { redirect_to settings_path, notice: 'Juror was successfully updated.' }
        format.js { render body: nil }
        format.json { render :show, status: :ok, location: @juror }
      else
        format.html { render "settings/modify" }
        format.json { render json: @juror.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jurors/1
  # DELETE /jurors/1.json
  def destroy
    @juror.destroy
    respond_to do |format|
      format.html { redirect_to settings_path, notice: 'Das Jury-Mitglied wurde erfolgreich entfernt.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_juror
    #   @juror = Juror.find(params[:id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def juror_params
      params.require(:juror).permit(:name, :participant_id)
    end
end
