class OrdersController < ApplicationController
  def new
    # Check if there are still tickets available
    if (@available_ticket_count = Settings.get.max_online_tickets - Ticket.online_tickets.count) <= 0
      flash[:alert] = "Leider sind alle Online-Tickets bereits ausverkauft."
      redirect_to root_url
    elsif !Settings.get.sale_of_tickets?
      flash[:alert] = "Leider ist die Online-Bestellfrist abgelaufen."
      redirect_to root_url
    else
      @ticket_order = TicketOrder.new
    end
  end

  def create
    @available_ticket_count = Settings.get.max_online_tickets - Ticket.online_tickets.count
    @ticket_order = TicketOrder.new(ticket_order_params)
    if @available_ticket_count - @ticket_order.number_of_tickets >= 0 && @ticket_order.number_of_tickets <= 20 && @ticket_order.save
      OrderMailer.confirmation_email(@ticket_order).deliver_now
      redirect_to orders_confirm_email_path
    else
      render :new
    end
  end

  def thankyou
    @ticket_order = TicketOrder.find_by(email_verification_token: params[:token])
    if @ticket_order
      render
    else
      render text: "Invalid token"
    end
  end

  def print_tickets
    order = TicketOrder.find_by(email_verification_token: params[:token])
    if order
      send_data TicketGenerator.generate_order_pdf(order).render, filename: "MSOHatTalent-Karten.pdf", type: 'application/pdf', disposition: 'inline'
    else
      render text: "Invalid token"
    end
  end

  def verify_email
    @ticket_order = TicketOrder.find_by(email_verification_token: params[:token])
    unless @ticket_order.email_verified?      
      # Create tickets
      Ticket.transaction do
        (@ticket_order.number_of_tickets - Ticket.where(ticket_order: @ticket_order).count).times do
          Ticket.create(ticket_order: @ticket_order)
        end
      end
      
      OrderMailer.ticket_email(@ticket_order).deliver_now

      @ticket_order.update_attribute :email_verified, true
    end
    redirect_to orders_thankyou_path(token: params[:token])
  end

  private
    def ticket_order_params
      params.require(:ticket_order).permit(:number_of_tickets, :email, :first_name, :last_name)
    end
end
