class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_active_item

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden }
      format.html { redirect_to main_app.root_url, :alert => exception.message }
    end
  end

  protected
    def set_active_item
      @active = nil
    end
end
