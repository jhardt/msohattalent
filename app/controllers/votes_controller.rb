class VotesController < ApplicationController
  authorize_resource

  # GET /votes
  # GET /votes.json
  def index
    round = params[:round] == 'final' ? 'final' : 'qualifying_round'
    max_participants_shown = params[:round] == 'final' ? 3 : 8
    @total_votes = Vote.where(round: round).count
    @participants = Participant.joins(:votes).where(votes: { round: round }).select("participants.*, COUNT(*) AS number_of_votes").group("participants.id").order("number_of_votes DESC").first(max_participants_shown)
    if params[:view] == 'audience_result_screen'
      @no_header = true
      render :audience_result_screen
    else
      render
    end
  end

  protected
    def set_active_item
      @active = :votes
    end
end
