class AudienceVotesController < ApplicationController
  include ApplicationHelper

  def new
    if !Settings.get.qualifying_round? && !Settings.get.final?
      flash[:alert] = "Zur Zeit ist keine Abstimmung aktiv. Bitte warten Sie noch einen Moment, bis die Abstimmung freigegeben wird."
      redirect_to root_url
    elsif params[:code].blank?
      render :enter_code
    elsif params[:code].length != 5 || (ticket = Ticket.find_by(code: params[:code])).blank?
      flash[:alert] = "Der eingegebene Code ist ungültig. Bitte versuchen Sie es erneut."
      render :enter_code
    elsif !ticket.checked_in?
      flash[:alert] = "Nur Tickets, die die Einlasskontrolle passiert haben, können abstimmen."
      render :enter_code
    elsif ticket.votes.where(round: Settings.get.round).any?
      flash[:alert] = "Sie haben bereits abgestimmt."
      render :enter_code
    else
      @round = Settings.get.round
      @participants = (Settings.get.qualifying_round? ? Participant.all.includes(:jurors) : Participant.includes(:jurors).where(is_finalist: true)).order(:position)
      render
    end
  end

  def create
    current_round = Settings.get.round
    if (!Settings.get.qualifying_round? && !Settings.get.final?) || params[:round].blank? || params[:round] != current_round
      flash[:alert] = "Die Abstimmung ist leider schon vorbei."
      redirect_to root_url
      return
    elsif params[:ticket_code].blank? || (ticket = Ticket.find_by(code: params[:ticket_code])).blank?
      flash[:alert] = "Ungültiger Code."
      redirect_to redirect_to new_audience_vote_path(code: params[:ticket_code])
      return
    elsif !ticket.checked_in?
      flash[:alert] = "Nur Tickets, die die Einlasskontrolle passiert haben, können abstimmen."
      redirect_to redirect_to new_audience_vote_path(code: params[:ticket_code])
      return
    elsif ticket.votes.where(round: current_round).any?
      flash[:alert] = "Sie haben bereits abgestimmt."
      redirect_to root_url
      return
    elsif params[:participant_ids].blank? || !params[:participant_ids].is_a?(Array) || params[:participant_ids].any? { |id| id.blank? || !is_number?(id) || Participant.find_by(id: id).blank? }
      flash[:alert] = "Ungültige Teilnehmer."
      redirect_to new_audience_vote_path(code: params[:ticket_code])
      return
    elsif params[:participant_ids].map!(&:to_i).uniq! != nil
      flash[:alert] = "Sie können für jede(n) Teilnehmer(in) nur einmal voten." 
      redirect_to new_audience_vote_path(code: params[:ticket_code])
      return
    elsif params[:participant_ids].length < (current_round == "qualifying_round" ? 4 : 1) || params[:participant_ids].length > (current_round == "qualifying_round" ? 8 : 3)
      flash[:alert] = "Bitte beachten Sie die Minimal- und Maximalzahl an Stimmen." 
      redirect_to new_audience_vote_path(code: params[:ticket_code])
      return
    else
      Vote.transaction do
        params[:participant_ids].each do |participant_id|
          Vote.create!(round: current_round, participant_id: participant_id, ticket_id: ticket.id)
        end
      end
      redirect_to audience_vote_thankyou_path
    end
  end

  def thankyou
  end
end
