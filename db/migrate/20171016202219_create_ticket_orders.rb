class CreateTicketOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :ticket_orders do |t|
      t.integer :number_of_tickets, null: false, default: 1
      t.string :email, null: false
      t.boolean :email_verified, null: false, default: false

      t.timestamps
    end
  end
end
