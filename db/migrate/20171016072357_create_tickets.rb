class CreateTickets < ActiveRecord::Migration[5.1]
  def change
    create_table :tickets do |t|
      t.string :code, null: false

      t.timestamps
    end
    add_index :tickets, :code, unique: true
  end
end
