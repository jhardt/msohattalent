class CreateSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :settings do |t|
      t.integer :max_online_tickets, null: false, default: 0
      t.integer :round, null: false, default: 0

      t.timestamps
    end
  end
end
