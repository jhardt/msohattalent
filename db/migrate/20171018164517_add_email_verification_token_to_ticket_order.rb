class AddEmailVerificationTokenToTicketOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :ticket_orders, :email_verification_token, :string, null: false, default: ""
    add_index :ticket_orders, :email_verification_token, unique: true
  end
end
