class AddAdmissionFeePaidToTicket < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :admission_fee_paid, :boolean, null: false, default: false
  end
end
