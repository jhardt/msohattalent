class AddPositionToParticipant < ActiveRecord::Migration[5.1]
  def change
    add_column :participants, :position, :integer, null: false, default: 0
  end
end
