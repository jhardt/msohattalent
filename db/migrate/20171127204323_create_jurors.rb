class CreateJurors < ActiveRecord::Migration[5.1]
  def change
    create_table :jurors do |t|
      t.string :name, null: false, default: ''
      t.references :participant, foreign_key: true

      t.timestamps
    end
    add_index :jurors, :name, unique: true
  end
end
