class CreateParticipants < ActiveRecord::Migration[5.1]
  def change
    create_table :participants do |t|
      t.string :name, null: false
      t.text :description, null: false, default: ''
      t.boolean :is_finalist, null: false, default: false
      t.string :yt_video_id

      t.timestamps
    end
  end
end
