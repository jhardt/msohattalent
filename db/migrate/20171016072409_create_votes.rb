class CreateVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :votes do |t|
      t.integer :round, null: false
      t.references :ticket, foreign_key: true, null: false
      t.references :participant, foreign_key: true, null: false

      t.timestamps
    end
  end
end
