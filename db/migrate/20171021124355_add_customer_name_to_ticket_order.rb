class AddCustomerNameToTicketOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :ticket_orders, :first_name, :string, null: false, default: ""
    add_column :ticket_orders, :last_name, :string, null: false, default: ""
  end
end
