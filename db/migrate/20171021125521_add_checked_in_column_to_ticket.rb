class AddCheckedInColumnToTicket < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :checked_in, :boolean, null: false, default: false
  end
end
